import React from 'react';
import { mount, shallow } from 'enzyme';
import App from '../components/App';

test('should render <App /> without user', () => {
  const wrapper = mount(<App user={""}/>);
  expect(wrapper.find("div")).toHaveLength(1);
});

test('should render <App /> with user', () => {
  const wrapper = mount(<App user="Hejsan@gmail.com" />);
  expect(wrapper.find("div")).toHaveLength(1);
});

test('call the internal method loginSuccessful', () => {
  const wrapper = shallow(<App user="" />);
  wrapper.instance().loginSuccessful("Hejsan@gmail.com");
  expect(wrapper.state("user")).toBe("Hejsan@gmail.com");
});

test('call the internal method logout', () => {
  const wrapper = shallow(<App user="Samir" />);
  wrapper.instance().logout();
  expect(wrapper.state("user")).toBe("");
});
