import * as localStorageUtils from '../utils/localStorage';

beforeEach(() =>{
  localStorage.clear();
});

afterEach(() =>{
  localStorage.clear();
});

test('should get the user from localStorage', () => {
  localStorageUtils.saveUserToLocalStorage("Hejsan123@gmail.com");
  expect(localStorageUtils.getUserFromLocalStorage()).toBe("Hejsan123@gmail.com");
});

test('should get empty user from localStorage', () => {
  localStorageUtils.removeUserFromLocalStorage();
  expect(localStorageUtils.getUserFromLocalStorage()).toBe("");
});
