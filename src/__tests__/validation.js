import { validateEmail, validatePassword } from '../utils/validation';

test('passes on valid email', () => {
  expect(validateEmail("hej@gmail.com")).toBe(true);
});

test('fails on invalid email', () => {
  expect(validateEmail("shouldnotwork")).toBe(false);
});

test('validates password: 8 chars, 1 uppercase, 1 digit', () => {
  expect(validatePassword("Hejsan123")).toBe(true);
});

test('invalid password: missing digit', () => {
  expect(validatePassword("Hejsan")).toBe(false);
});

test('invalid password: missing 1 uppercase', () => {
  expect(validatePassword("hejsan123")).toBe(false);
});
