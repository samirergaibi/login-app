import React from 'react';
import { mount } from 'enzyme';
import Login from '../components/Login';

test('simulate login failed', () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);

  wrapper.find("input[name='email']").simulate("change", { target: { name: "email", value: "bademail" } });
  wrapper.find("input[name='password']").simulate("change", { target: { name: "password", value: "badpass" } });
  wrapper.find("form").simulate("submit");
  expect(wrapper.state("message").type).toBe("ERROR");
});

test('simulate login success', () => {
  const fakeLogin = jest.fn();
  const wrapper = mount(<Login loginSuccessful={fakeLogin} />);

  wrapper.find('input[name="email"]').simulate("change", { target: { name: "email", value: "hejsan@gmail.com" } });
  wrapper.find('input[name="password"]').simulate("change", { target: { name: "password", value: "Hejsan123" } });
  wrapper.simulate("submit");
  expect(wrapper.state("message").type).toBe("SUCCESS");
});
