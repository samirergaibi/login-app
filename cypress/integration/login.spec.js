
before(() => {
  cy.clearLocalStorage();
});

describe("Login test", () => {
  it("visit my localhost", () => {
    cy.visit("/");

    cy.get("input[name='email']")
      .type("user.name@domain.com")
      .should("have.value", "user.name@domain.com");

    cy.get("input[name='password']")
      .type("Hejsan123")
      .should("have.value", "Hejsan123");

    cy.get("input[type='submit']")
      .click();

    cy.get(".user")
      .should("contain", "user.name@domain.com");

  });
});